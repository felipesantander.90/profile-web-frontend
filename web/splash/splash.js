function removeSplashFromWeb() {
  console.log("Removing splash from web");
  document.getElementById("splash")?.remove();
  document.getElementById("splash-branding")?.remove();
  document.body.style.background = "transparent";
}

