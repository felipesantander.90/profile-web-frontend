import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/adapter/init.dart';
import 'package:template_flutter_web/providers/index.dart';
import 'package:template_flutter_web/routers/router.dart';
import 'package:template_flutter_web/strings/index.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

Future main() async {
  await dotenv.load(
      fileName:
          "lib/assets/.env"); // mergeWith optional, you can include Platform.environment for Mobile/Desktop app
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: indexProvider, child: const InitAdapter());
  }
}

class InitApp extends StatefulWidget {
  final LanguageCore languageActive;
  const InitApp({super.key, required this.languageActive});

  @override
  State<InitApp> createState() => _InitAppState();
}

class _InitAppState extends State<InitApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: routes,
      title: widget.languageActive.appTittle,
      theme: lightTheme,
      initialRoute: PathRouter.splashScreen,
    );
  }
}
