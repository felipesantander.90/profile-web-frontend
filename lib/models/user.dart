// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

bool debug = dotenv.get('DEBUG').toLowerCase() == 'true';

class UserModel {
  String name;
  String lastName;
  int age;
  String image;
  String imageLand;
  UserModel({
    required this.name,
    required this.lastName,
    required this.age,
    required this.image,
    required this.imageLand,
  });

  UserModel copyWith({
    String? name,
    String? lastName,
    int? age,
    String? image,
    String? imageLand,
  }) {
    return UserModel(
      name: name ?? this.name,
      lastName: lastName ?? this.lastName,
      age: age ?? this.age,
      image: image ?? this.image,
      imageLand: imageLand ?? this.imageLand,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'lastName': lastName,
      'age': age,
      'image': image,
      'imageLand': imageLand,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      name: map['name'] as String,
      lastName: map['last_name'] as String,
      age: map['age'] as int,
      image: debug
          ? map['image'] as String
          : (map['image'] as String).replaceAll('http://', 'https://'),
      imageLand: debug
          ? map['land_image'] as String
          : (map['land_image'] as String).replaceAll('http://', 'https://'),
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'UserModel(name: $name, lastName: $lastName, age: $age, image: $image, imageLand: $imageLand)';
  }

  @override
  bool operator ==(covariant UserModel other) {
    if (identical(this, other)) return true;

    return other.name == name &&
        other.lastName == lastName &&
        other.age == age &&
        other.image == image &&
        other.imageLand == imageLand;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        lastName.hashCode ^
        age.hashCode ^
        image.hashCode ^
        imageLand.hashCode;
  }
}
