// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:template_flutter_web/models/contactType.dart';

class Contact {
  String id;
  String contact;
  ContactType? contactType;
  Contact({
    required this.id,
    required this.contact,
    this.contactType,
  });

  Contact copyWith({
    String? id,
    String? contact,
  }) {
    return Contact(
      id: id ?? this.id,
      contact: contact ?? this.contact,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'contact': contact,
      'contactType': contactType?.toMap(),
    };
  }

  factory Contact.fromMap(Map<String, dynamic> map) {
    return Contact(
      id: map['id'] as String,
      contact: map['contact'] as String,
      contactType: map['type_contact'] != null
          ? ContactType.fromMap(map['type_contact'] as Map<String, dynamic>)
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Contact.fromJson(String source) =>
      Contact.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'Contact(id: $id, contact: $contact, contactType: $contactType)';

  @override
  bool operator ==(covariant Contact other) {
    if (identical(this, other)) return true;

    return other.id == id && other.contact == contact;
  }

  @override
  int get hashCode => id.hashCode ^ contact.hashCode;
}
