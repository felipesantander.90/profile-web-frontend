// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class SpeecheModel {
  String id;
  String title;
  String description;
  String url;
  SpeecheModel({
    required this.id,
    required this.title,
    required this.description,
    required this.url,
  });

  SpeecheModel copyWith({
    String? id,
    String? title,
    String? description,
    String? url,
  }) {
    return SpeecheModel(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
      url: url ?? this.url,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'title': title,
      'description': description,
      'url': url,
    };
  }

  factory SpeecheModel.fromMap(Map<String, dynamic> map) {
    return SpeecheModel(
      id: map['id'] as String,
      title: map['title'] as String,
      description: map['description'] as String,
      url: map['image'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory SpeecheModel.fromJson(String source) =>
      SpeecheModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Speeche(id: $id, title: $title, description: $description, url: $url)';
  }

  @override
  bool operator ==(covariant SpeecheModel other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.title == title &&
        other.description == description &&
        other.url == url;
  }

  @override
  int get hashCode {
    return id.hashCode ^ title.hashCode ^ description.hashCode ^ url.hashCode;
  }
}
