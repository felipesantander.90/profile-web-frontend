// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class ContactType {
  String id;
  String name;
  String icon;
  ContactType({
    required this.id,
    required this.name,
    required this.icon,
  });

  ContactType copyWith({
    String? id,
    String? name,
    String? icon,
  }) {
    return ContactType(
      id: id ?? this.id,
      name: name ?? this.name,
      icon: icon ?? this.icon,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'icon': icon,
    };
  }

  factory ContactType.fromMap(Map<String, dynamic> map) {
    return ContactType(
      id: map['id'] as String,
      name: map['name'] as String,
      icon: map['icon'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory ContactType.fromJson(String source) =>
      ContactType.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'ContactType(id: $id, name: $name, icon: $icon)';

  @override
  bool operator ==(covariant ContactType other) {
    if (identical(this, other)) return true;

    return other.id == id && other.name == name && other.icon == icon;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ icon.hashCode;
}
