import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:template_flutter_web/api/profile.dart';
import 'package:template_flutter_web/models/contact.dart';
import 'package:template_flutter_web/models/speeche.dart';
import 'package:template_flutter_web/models/user.dart';
import 'dart:convert';

class UserProvider extends ChangeNotifier with AccountService {
  dynamic data;
  UserModel? userProfile;
  List<Contact> userContacts = [];
  List<String> userSpeches = [];

  void saveInfoUser(Response response) {
    var jsonBody = json.decode(response.body);
    data = jsonBody;
  }

  List<Contact> makeUserContacts() {
    var userContactsJson = data['user_conctats'];
    List<Contact> userContacts = [];
    userContactsJson.forEach((element) {
      userContacts.add(Contact.fromMap(element));
    });
    this.userContacts = userContacts;
    return userContacts;
  }

  List<String> makeUserSpeeches() {
    var userSpeechesJson = data['user_speeches'];
    List<String> userSpeeches = [];
    userSpeechesJson.forEach((element) {
      userSpeeches.add(element["id"]);
    });
    userSpeches = userSpeeches;
    return userSpeeches;
  }

  UserModel? makeInfoUSer() {
    var userInfoJson = data['user_profile'];
    userProfile = UserModel.fromMap(userInfoJson);
    return userProfile;
  }

  Future<Response> getUserInfo(String language) async {
    return await fetchInfoUser(language);
  }

  Future<Response> getInfoUserSpeeche(String language, String id) async {
    return await fetchInfoUserSpeeche(language, id);
  }

  SpeecheModel makeUserSpeeche(Response response) {
    var jsonBody = json.decode(response.body);
    var speeche = SpeecheModel.fromMap(jsonBody);
    return speeche;
  }
}
