import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:template_flutter_web/api/language.dart';
import 'package:template_flutter_web/models/language.dart';
import 'dart:convert';

import 'package:template_flutter_web/strings/index.dart';

var languages = {
  "english": "en",
  "spanish": "es",
};

class LanguageProvider extends ChangeNotifier with LanguageService {
  LanguageModel? selectLanguage;
  List<LanguageModel> listLanguage = [];

  void setLanguage(LanguageModel language) {
    selectLanguage = language;
    notifyListeners();
  }

  String getLanguage() {
    return languages[selectLanguage!.name]!;
  }

  LanguageCore getLanguageSelected() {
    LanguageCore core =
        LanguageCore(language: languages[selectLanguage?.name] ?? "es");
    return core;
  }

  void saveLanguage(Response response) {
    var jsonBody = json.decode(response.body);
    listLanguage =
        (jsonBody as List).map((e) => LanguageModel.fromMap(e)).toList();
    if (listLanguage.isNotEmpty) {
      selectLanguage = listLanguage.elementAt(0);
    }
  }

  Future<Response> getLanguages() async {
    return await fetchLanguage();
  }
}
