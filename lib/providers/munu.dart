import 'package:flutter/material.dart';
import 'package:template_flutter_web/components/buttonMenu/index.dart';
import 'package:template_flutter_web/strings/index.dart';
import 'package:template_flutter_web/views/profile/index.dart';

class MenuProvider extends ChangeNotifier {
  String? titleMenuSelected = Esp.menuButton1;
  Widget? menuSelected = const Profile();
  late List menuItems;
  void setMenuSelected(String title) {
    titleMenuSelected = title;
    menuSelected =
        menuItems.firstWhere((item) => item['title'] == title)['Widget'];
    notifyListeners();
  }

  List<Widget> createMenu() {
    return menuItems
        .map(
          (item) => ButtonMenu(
              title: item['title'],
              icon: item['icon'],
              onTap: () {
                setMenuSelected(item['title']);
              },
              titleMenuSelected: titleMenuSelected),
        )
        .toList();
  }
}
