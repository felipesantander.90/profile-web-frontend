import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:template_flutter_web/providers/language.dart';
import 'package:template_flutter_web/providers/munu.dart';
import 'package:template_flutter_web/providers/user.dart';

List<SingleChildWidget> indexProvider = [
  ChangeNotifierProvider<LanguageProvider>(
    create: (context) => LanguageProvider(),
  ),
  ChangeNotifierProvider<UserProvider>(
    create: (context) => UserProvider(),
  ),
  ChangeNotifierProvider<MenuProvider>(
    create: (context) => MenuProvider(),
  ),
];
