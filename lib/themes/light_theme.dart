import 'dart:ui';

import 'package:flutter/material.dart';

makeColor(String str) => Color(int.parse("0xFF${str.replaceAll('#', '')}"));

Color backGroundColor = makeColor("#276e90");
Color firstColor = makeColor("#cecfc9");
Color secondColor = makeColor("#0a3143");
Color thirdColor = makeColor("#f2aa1f");
Color fontColor1 = Colors.white70;
Color fontColor2 = Colors.white60;
Color fontColor3 = Colors.white54;

// a gradiont decoration color
gradientDecoration(context) => BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        tileMode: TileMode.mirror,
        colors: [
          Theme.of(context).backgroundColor,
          Theme.of(context).backgroundColor.withOpacity(.95),
        ],
      ),
    );

final lightTheme = ThemeData(
  fontFamily: 'Lato',
  inputDecorationTheme: InputDecorationTheme(
    contentPadding: const EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
    hintStyle: TextStyle(
        fontFeatures: const [FontFeature.proportionalFigures()],
        fontSize: 15.0,
        color: fontColor1),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: firstColor, width: 2.0),
    ),
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: fontColor1, width: 2.0),
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
          elevation: MaterialStateProperty.all<double>(5.0),
          padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
              const EdgeInsets.symmetric(vertical: 14.0)),
          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
          backgroundColor: MaterialStateProperty.all<Color>(firstColor),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  side: BorderSide(color: thirdColor))))),
  primaryColor: firstColor,
  backgroundColor: backGroundColor,
  textTheme: TextTheme(
    headline1: TextStyle(color: fontColor1),
    headline2: TextStyle(color: fontColor1),
    headline3: TextStyle(color: fontColor1),
    headline4: TextStyle(color: fontColor1),
    headline5: TextStyle(color: fontColor1),
    headline6: TextStyle(color: fontColor1),
    bodyText1: TextStyle(color: fontColor2),
    bodyText2: TextStyle(color: fontColor1),
    subtitle1: TextStyle(color: fontColor3),
    subtitle2: TextStyle(color: fontColor3),
  ),
);
