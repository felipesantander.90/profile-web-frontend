import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:template_flutter_web/api/baseAPi.dart';

abstract class LanguageService {
  String epLanguages = 'language/';
  BaseApi baseApi = BaseApi();

  Future<http.Response> fetchLanguage() async {
    final response = baseApi.debug
        ? await http.get(
            Uri.http(
                "${baseApi.urlBackend}:${baseApi.portBackend}", epLanguages),
            headers: {
                "Accept": "application/json",
                "Acces-Control_Allow_Origin": "*"
              })
        : await http.get(Uri.https(baseApi.urlBackend, epLanguages), headers: {
            "Accept": "application/json",
            "Acces-Control_Allow_Origin": "*"
          });
    return response;
  }
}
