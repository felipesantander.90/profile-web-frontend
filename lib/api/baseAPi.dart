import 'package:flutter_dotenv/flutter_dotenv.dart';

class BaseApi {
  bool debug = dotenv.get('DEBUG').toLowerCase() == 'true';
  String urlBackend = dotenv.get('URL_BACKEND', fallback: 'localhost');
  String portBackend = dotenv.get('POR_BACKEND', fallback: '9000');
}
