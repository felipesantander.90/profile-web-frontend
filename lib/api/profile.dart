import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:template_flutter_web/api/baseAPi.dart';

abstract class AccountService {
  String epInfoUSer(language) => '$language/user_profile/';
  String epInfoUSerSpeeche(language, id) =>
      '$language/user_profile/speeche/$id';

  BaseApi baseApi = BaseApi();
  Future<http.Response> fetchInfoUser(String language) async {
    final response = baseApi.debug
        ? await http.get(
            Uri.http("${baseApi.urlBackend}:${baseApi.portBackend}",
                epInfoUSer(language)),
            headers: {
                "Accept": "application/json",
                "Acces-Control_Allow_Origin": "*"
              })
        : await http.get(Uri.https(baseApi.urlBackend, epInfoUSer(language)),
            headers: {
                "Accept": "application/json",
                "Acces-Control_Allow_Origin": "*"
              });
    return response;
  }

  Future<http.Response> fetchInfoUserSpeeche(String language, String id) async {
    final response = baseApi.debug
        ? await http.get(
            Uri.http("${baseApi.urlBackend}:${baseApi.portBackend}",
                epInfoUSerSpeeche(language, id)),
            headers: {
                "Accept": "application/json",
                "Acces-Control_Allow_Origin": "*"
              })
        : await http.get(
            Uri.https(baseApi.urlBackend, epInfoUSerSpeeche(language, id)),
            headers: {
                "Accept": "application/json",
                "Acces-Control_Allow_Origin": "*"
              });
    return response;
  }
}
