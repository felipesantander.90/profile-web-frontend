import 'package:flutter/material.dart';
import 'package:gradient_progress_indicator/gradient_progress_indicator.dart';
import 'package:template_flutter_web/strings/index.dart';

class CircularCharger extends StatelessWidget {
  const CircularCharger({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: GradientProgressIndicator(
        radius: 72,
        duration: 2,
        strokeWidth: 8,
        gradientStops: const [
          0.3,
          0.7,
        ],
        gradientColors: const [
          Colors.grey,
          Colors.green,
        ],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              Assets.companyIcon,
              fit: BoxFit.fitWidth,
              width: 120,
              height: 120,
            )
          ],
        ),
      ),
    ));
  }
}
