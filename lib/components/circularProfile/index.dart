import 'package:flutter/material.dart';
import 'package:widget_circular_animator/widget_circular_animator.dart';

class CircularProfile extends StatelessWidget {
  final String? urlImage;
  final Color outerColor;
  final Color innerColor;
  final double outerRadius;
  const CircularProfile({
    super.key,
    this.urlImage,
    this.outerColor = Colors.white,
    this.innerColor = Colors.white,
    this.outerRadius = 100.0,
  });

  @override
  Widget build(BuildContext context) {
    return WidgetCircularAnimator(
      size: outerRadius,
      innerIconsSize: 4,
      outerIconsSize: 3,
      innerAnimation: Curves.decelerate,
      outerAnimation: Curves.slowMiddle,
      innerColor: innerColor,
      outerColor: outerColor,
      innerAnimationSeconds: 10,
      outerAnimationSeconds: 8,
      child: ClipOval(
        child: Image.network(
          urlImage!,
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
