import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

class ButtonMenu extends StatefulWidget {
  final String title;
  final String icon;
  final String? titleMenuSelected;
  final Function onTap;
  const ButtonMenu(
      {Key? key,
      required this.title,
      required this.icon,
      required this.titleMenuSelected,
      required this.onTap})
      : super(key: key);

  @override
  State<ButtonMenu> createState() => _ButtonMenuState();
}

class _ButtonMenuState extends State<ButtonMenu> {
  Color colorBackground = Colors.white.withOpacity(.2);
  Color colorText = thirdColor;
  bool enter = false;
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    final width = mediaQueryData.size.width;
    var text = Text(widget.title,
        softWrap: false,
        style:
            Theme.of(context).textTheme.bodyText1!.copyWith(color: fontColor1));
    var icon = SvgPicture.asset(
      widget.icon,
      width: width < 600 ? 20 : 30,
      height: width < 600 ? 20 : 30,
      color: thirdColor,
    );
    textAndIcon(enter) => Row(children: [
          icon,
          SizedBox(
            width: enter || widget.titleMenuSelected == widget.title ? 100 : 0,
            child: Row(
              children: [const SizedBox(width: 10), text],
            ),
          )
        ]);

    return MouseRegion(
        onEnter: (event) {
          setState(() {
            enter = true;
            colorBackground = backGroundColor;
            colorText = fontColor1;
          });
        },
        onExit: (event) {
          setState(() {
            enter = false;
            colorBackground = Colors.white.withOpacity(.2);
            colorText = thirdColor;
          });
        },
        child: GestureDetector(
          onTap: (() {
            widget.onTap();
          }),
          child: AnimatedSize(
            curve: Curves.linear,
            duration: const Duration(milliseconds: 100),
            child: Container(
              width:
                  enter || widget.titleMenuSelected == widget.title ? 160 : 60,
              height: 50,
              margin: const EdgeInsets.only(bottom: 30),
              padding: const EdgeInsets.only(left: 15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(
                      color: enter || widget.titleMenuSelected == widget.title
                          ? fontColor1
                          : Colors.transparent,
                      width: .4),
                  color: secondColor),
              child: AnimatedSize(
                  duration: const Duration(milliseconds: 800),
                  child: textAndIcon(enter)),
            ),
          ),
        ));
  }
}
