import 'package:flutter/material.dart';
import 'package:template_flutter_web/models/speeche.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

class Speeche extends StatelessWidget {
  final SpeecheModel? speeche;
  const Speeche({super.key, required this.speeche});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 230,
        margin: const EdgeInsets.only(bottom: 20),
        child: IntrinsicHeight(
            child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 3,
                child: Container(
                  padding: const EdgeInsets.only(top: 50, left: 40, right: 20),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: secondColor.withOpacity(.2),
                            offset: const Offset(0, 2),
                            blurRadius: 5)
                      ],
                      color: secondColor,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(15),
                        bottomLeft: Radius.circular(15),
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(speeche!.title,
                          style: Theme.of(context)
                              .textTheme
                              .headline5!
                              .copyWith(fontWeight: FontWeight.bold)),
                      const SizedBox(height: 30),
                      ConstrainedBox(
                          constraints: const BoxConstraints(maxWidth: 750),
                          child: Text(speeche!.description,
                              style: Theme.of(context).textTheme.bodyText1!,
                              overflow: TextOverflow.clip,
                              maxLines: 4,
                              textAlign: TextAlign.left)),
                    ],
                  ),
                )),
            Expanded(
              flex: 1,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(15),
                    topRight: Radius.circular(15)),
                child:
                    Image.network(height: 230, speeche!.url, fit: BoxFit.cover),
              ),
            ),
          ],
        )));
  }
}
