import 'package:flutter/material.dart';

const hintStyle = TextStyle(
  fontSize: 14,
  color: Colors.grey,
);

inputForm(controler, validator, node) => TextFormField(
      onEditingComplete: () => node.nextFocus(),
      controller: controler,
      validator: validator,
      decoration: const InputDecoration(
        hintText: "Email",
        hintStyle: hintStyle,
      ),
    );
