import 'package:line_icons/line_icons.dart';

class Errors {
  static const reIntentEs = 'Ocurrio un error Intente mas tarde';
  static const errorCheckLoginEs =
      "Algo salio mal. Usuario o Contraseña incorrectas";
}

class Esp {
  static const appTittle = 'Perfil';
  static const menuButton1 = 'Perfil';
  static const menuButton2 = 'Habilidades';
  static const menuButton3 = 'Experiencia';
  static const menuButton4 = 'Hobbies';
  static const menuButton5 = 'Contacto';
  static printAge(age) => '$age años';
  //errors
  static const reIntent = Errors.reIntentEs;
  static const errorCheckLogin = Errors.errorCheckLoginEs;
}

class En {
  static const appTittle = 'profile';
  static const menuButton1 = 'profile';
  static const menuButton2 = 'Habilities';
  static const menuButton3 = 'Experience';
  static const menuButton4 = 'Hobbies';
  static const menuButton5 = 'Contact';
  static printAge(age) => '$age years old';
  //errors
  static const reIntent = Errors.reIntentEs;
  static const errorCheckLogin = Errors.errorCheckLoginEs;
}

class Assets {
  static String companyIcon = 'lib/assets/common/companyIcon.png';
  static String loginWallpaper = 'lib/assets/login/loginWallpaper.webp';
  static String chip = 'lib/assets/common/chip.png';
  static String migration = 'lib/assets/common/migration.png';
  static String example = 'lib/assets/common/example.png';
  static String anatomy = 'lib/assets/icons/anatomy.svg';
  static String habilities = 'lib/assets/icons/bookmarklet.svg';
  static String angelWings = 'lib/assets/icons/angel_wings.svg';
  static String battleGear = 'lib/assets/icons/battle-gear.svg';
  static String diamondHard = 'lib/assets/icons/diamond-hard.svg';
  static String mazeSaw = 'lib/assets/icons/maze-saw.svg';
  static String openTreasureChest = 'lib/assets/icons/open-treasure-chest.svg';
  static String ram = 'lib/assets/icons/ram.svg';
  static String warlordHelmet = 'lib/assets/icons/warlord-helmet.svg';
  static String arrowScope = 'lib/assets/icons/arrow-scope.svg';
}

Map internalIcons = {
  "university": LineIcons.university,
  "email": LineIcons.envelope,
  "phone": LineIcons.phone,
  "age": LineIcons.birthdayCake,
  "job": LineIcons.briefcase,
};

class LanguageCore {
  final String? language;
  LanguageCore({this.language});
  String get appTittle => language == 'es' ? Esp.appTittle : En.appTittle;
  String get menuButton1 => language == 'es' ? Esp.menuButton1 : En.menuButton1;
  String get menuButton2 => language == 'es' ? Esp.menuButton2 : En.menuButton2;
  String get menuButton3 => language == 'es' ? Esp.menuButton3 : En.menuButton3;
  String get menuButton4 => language == 'es' ? Esp.menuButton4 : En.menuButton4;
  String get menuButton5 => language == 'es' ? Esp.menuButton5 : En.menuButton5;
  String get reIntent => language == 'es' ? Esp.reIntent : En.reIntent;
  String get errorCheckLogin =>
      language == 'es' ? Esp.errorCheckLogin : En.errorCheckLogin;
  String printAge(age) =>
      language == 'es' ? Esp.printAge(age) : En.printAge(age);
}
