import 'package:flutter/material.dart';
import 'package:template_flutter_web/loaders/figures/circle.dart';
import 'package:template_flutter_web/loaders/figures/square.dart';

class BodyLoading extends StatefulWidget {
  const BodyLoading({super.key});

  @override
  State<BodyLoading> createState() => _BodyLoadingState();
}

class _BodyLoadingState extends State<BodyLoading> {
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    final width = mediaQueryData.size.width;
    final height = mediaQueryData.size.height;
    return Stack(alignment: Alignment.center, children: [
      // background image and bottom contents
      Column(
        children: [
          squareLoading(width, 200.0),
          Expanded(
            child: Container(
              color: Colors.transparent,
              child: Center(),
            ),
          )
        ],
      ),
      // Profile image
      Positioned(
        left: width > 600 ? 200 : width / 2 - 135,
        top: 70.0, // (background container size) - (circle height / 2)
        child: circleLoading(200, 200),
      )
    ]);
  }
}
