import 'package:skeletons/skeletons.dart';

SkeletonAvatar squareLoading(width, height) =>
    SkeletonAvatar(style: SkeletonAvatarStyle(width: width, height: height));
