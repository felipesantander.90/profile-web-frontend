import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

SkeletonAvatar circleLoading(width, height) => SkeletonAvatar(
    style: SkeletonAvatarStyle(
        shape: BoxShape.circle, width: width, height: height));
