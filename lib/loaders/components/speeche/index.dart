import 'package:flutter/material.dart';
import 'package:template_flutter_web/loaders/figures/square.dart';
import 'package:template_flutter_web/models/speeche.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

class SpeecheLoader extends StatelessWidget {
  const SpeecheLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 230,
        margin: const EdgeInsets.only(bottom: 20),
        child: IntrinsicHeight(
            child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                flex: 3,
                child: Container(
                  padding: const EdgeInsets.only(top: 50, left: 40, right: 20),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: secondColor.withOpacity(.2),
                            offset: const Offset(0, 2),
                            blurRadius: 5)
                      ],
                      color: secondColor,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(15),
                        bottomLeft: Radius.circular(15),
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      squareLoading(300, 20),
                      const SizedBox(height: 30),
                      squareLoading(double.infinity, 100),
                    ],
                  ),
                )),
            Expanded(
              flex: 1,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(15),
                    topRight: Radius.circular(15)),
                child: squareLoading(230, 230),
              ),
            ),
          ],
        )));
  }
}
