import 'package:flutter/material.dart';
import 'package:template_flutter_web/components/listSpeeches/index.dart';
import 'package:template_flutter_web/models/contact.dart';
import 'package:template_flutter_web/models/user.dart';
import 'package:template_flutter_web/strings/index.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

import '../../components/circularProfile/index.dart';

class Body extends StatefulWidget {
  final LanguageCore language;
  final UserModel? userProfile;
  final List<Contact> userContacts;
  const Body(
      {super.key,
      required this.language,
      required this.userContacts,
      this.userProfile});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    final mediaQueryData = MediaQuery.of(context);
    final width = mediaQueryData.size.width;
    Contact mailContact = widget.userContacts
        .firstWhere((element) => element.contactType!.name == 'email');
    return Stack(alignment: Alignment.center, children: [
      // background image and bottom contents
      Column(
        children: [
          ShaderMask(
            shaderCallback: (rect) {
              return const LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.red, Colors.transparent],
              ).createShader(Rect.fromLTRB(190, 190, rect.width, rect.height));
            },
            blendMode: BlendMode.dstIn,
            child: Image.network(
              height: 200,
              width: width,
              widget.userProfile!.imageLand,
              fit: BoxFit.cover,
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(
                left: screenSize.width * 0.1,
                right: screenSize.width * 0.1,
                top: screenSize.height * 0.12,
              ),
              color: Colors.transparent,
              child: const ListSpecches(),
            ),
          )
        ],
      ),
      // Profile image
      Positioned(
        left: width > 600 ? 230 : width / 2 - 135,
        top: 105.0, // (background container size) - (circle height / 2)
        child: Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(100),
              topRight: Radius.circular(20),
              bottomLeft: Radius.circular(100),
              bottomRight: Radius.circular(20),
            ),
            color: secondColor,
          ),
          width: 400,
          height: 130,
        ),
      ),

      Positioned(
        left: width > 600 ? 200 : width / 2 - 135,
        top: 70.0, // (background container size) - (circle height / 2)
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CircularProfile(
              outerRadius: 200,
              outerColor: firstColor,
              innerColor: thirdColor,
              urlImage: widget.userProfile!.image,
            ),
            const SizedBox(
              width: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      "${widget.userProfile!.name} ${widget.userProfile!.lastName}",
                      style: Theme.of(context)
                          .textTheme
                          .headline6!
                          .copyWith(fontWeight: FontWeight.bold)),
                  const SizedBox(
                    height: 5,
                  ),
                  subTex(context, 'developer', internalIcons['university']),
                  const SizedBox(
                    height: 5,
                  ),
                  subTex(context, mailContact.contact,
                      internalIcons[mailContact.contactType!.icon]),
                  const SizedBox(
                    height: 5,
                  ),
                  subTex(
                      context,
                      widget.language.printAge(widget.userProfile!.age),
                      internalIcons['age']),
                ],
              ),
            )
          ],
        ),
      )
    ]);
  }
}

subTex(context, text, icon) => Row(
      children: [
        Icon(
          icon,
          color: thirdColor,
          size: 20,
        ),
        const SizedBox(
          width: 5,
        ),
        Text(text,
            style: Theme.of(context).textTheme.subtitle2!.copyWith(
                  fontStyle: FontStyle.italic,
                ))
      ],
    );
