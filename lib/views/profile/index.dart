import 'package:flutter/material.dart';
import 'package:template_flutter_web/adapter/views/profile.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return const ProfileAdapter();
  }
}
