import 'package:flutter/material.dart';
import 'package:template_flutter_web/adapter/components/menu/index.dart';
import 'package:template_flutter_web/views/home/body.dart';

import '../../themes/light_theme.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
        body: Container(
      decoration: gradientDecoration(context),
      child: Stack(children: [
        const Body(),
        Positioned(
          top: screenSize.height * 0.15,
          left: screenSize.width * 0.004,
          child: Container(
            height: 500,
            width: screenSize.width * 0.025,
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      blurRadius: 100,
                      spreadRadius: 20)
                ],
                border: Border.all(color: firstColor.withOpacity(.8)),
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(10))),
          ),
        ),
        Positioned(
            left: screenSize.width * 0.012,
            top: screenSize.height * 0.2,
            child: SizedBox(
              width: screenSize.width * 0.3,
              child: const MenuAdapter(),
            )),
      ]),
    ));
  }
}
