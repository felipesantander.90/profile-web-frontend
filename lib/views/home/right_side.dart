import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/providers/munu.dart';

class RightSide extends StatefulWidget {
  const RightSide({Key? key}) : super(key: key);

  @override
  State<RightSide> createState() => _RightSideState();
}

class _RightSideState extends State<RightSide> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    MenuProvider menu = Provider.of<MenuProvider>(context, listen: false);
    return Stack(
      children: [
        Container(
          width: screenSize.width * 0.03,
          padding: const EdgeInsets.only(left: 12, right: 12, top: 20),
          height: screenSize.height,
          color: Theme.of(context).primaryColor,
        ),
        Positioned(
            child: Column(
          children: menu.createMenu(),
        ))
      ],
    );

    // Container(
    // padding: const EdgeInsets.only(left: 12, right: 12, top: 20),
    // height: screenSize.height,
    // color: Theme.of(context).primaryColor,
    // child: Column(
    //   mainAxisAlignment: MainAxisAlignment.center,
    //   crossAxisAlignment: CrossAxisAlignment.center,
    //   children: menu.createMenu(),
    // ));
  }
}
