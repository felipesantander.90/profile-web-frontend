import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/providers/munu.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    MenuProvider menuProvider =
        Provider.of<MenuProvider>(context, listen: true);

    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 500),
      transitionBuilder: (Widget child, Animation<double> animation) {
        return FadeTransition(opacity: animation, child: child);
      },
      child: SizedBox(
        height: screenSize.height,
        child: menuProvider.menuSelected,
      ),
    );
  }
}
