import 'package:template_flutter_web/views/home/index.dart';
import 'package:template_flutter_web/views/splashScreen/index.dart';

class PathRouter {
  static const String splashScreen = "/";
  static const String profile = "/profile";
}

var routes = {
  PathRouter.splashScreen: (context) => const SplashScreen(),
  PathRouter.profile: (context) => const Dashboard(),
};
