import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/adapter/components/speeche/index.dart';
import 'package:template_flutter_web/providers/user.dart';
import 'package:template_flutter_web/strings/index.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

class ListSpeechesAdapter extends StatelessWidget {
  const ListSpeechesAdapter({super.key});

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);

    return SingleChildScrollView(
        child: Column(
      children: userProvider.userSpeches
          .map((e) => SpeecheAdapter(speecheId: e))
          .toList(),
    ));
  }
}

Widget speeche(context) => Container(
    height: 230,
    margin: EdgeInsets.only(bottom: 20),
    child: IntrinsicHeight(
        child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
            flex: 3,
            child: Container(
              padding: const EdgeInsets.only(top: 50, left: 40, right: 20),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: secondColor.withOpacity(.2),
                        offset: const Offset(0, 2),
                        blurRadius: 5)
                  ],
                  color: secondColor,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(50),
                    bottomLeft: Radius.circular(50),
                  )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Vision general de mi perfil',
                      style: Theme.of(context)
                          .textTheme
                          .headline5!
                          .copyWith(fontWeight: FontWeight.bold)),
                  const SizedBox(height: 30),
                  Text(
                      """esto es el cuerpo de un discurso para ejemeplificar el diseño de la pagina, intentando que se vea lo mas parecido a la realidad posible esto es el cuerpo de un discurso para ejemeplificar el diseño de la pagina, intentando que se vea lo mas parecido a la realidad posible esto es el cuerpo de un discurso para ejemeplificar el diseño de la pagina, intentando que se vea lo mas parecido a la realidad posible esto es el cuerpo de un discurso para ejemeplificar el diseño de la pagina, intentando que se vea lo mas parecido a la realidad posible
  esto es el cuerpo de un discurso para ejemeplificar el diseño de la pagina, intentando que se vea lo mas parecido a la realidad posible
  esto es el cuerpo de un discurso para ejemeplificar el diseño de la pagina, intentando que se vea lo mas parecido a la realidad posible
  esto es el cuerpo de un discurso para ejemeplificar el diseño de la pagina, intentando que se vea lo mas parecido a la realidad posible
            """,
                      style: Theme.of(context).textTheme.bodyText1!,
                      overflow: TextOverflow.clip,
                      maxLines: 4,
                      textAlign: TextAlign.left),
                ],
              ),
            )),
        Expanded(
          flex: 1,
          child: ClipRRect(
            borderRadius: const BorderRadius.only(
                bottomRight: Radius.circular(50),
                topRight: Radius.circular(50)),
            child: Image.asset(Assets.example, fit: BoxFit.fill),
          ),
        ),
      ],
    )));
