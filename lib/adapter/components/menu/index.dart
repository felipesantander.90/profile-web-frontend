import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/providers/language.dart';
import 'package:template_flutter_web/providers/munu.dart';

import '../../../strings/index.dart';
import '../../../views/profile/index.dart';

class MenuAdapter extends StatelessWidget {
  const MenuAdapter({super.key});

  @override
  Widget build(BuildContext context) {
    MenuProvider menuProvider =
        Provider.of<MenuProvider>(context, listen: true);
    LanguageProvider languageProvider =
        Provider.of<LanguageProvider>(context, listen: true);
    LanguageCore language = languageProvider.getLanguageSelected();
    menuProvider.menuItems = [
      {
        'title': language.menuButton1,
        'icon': Assets.warlordHelmet,
        'Widget': const Profile(),
      },
      // {
      //   'title': language.menuButton2,
      //   'icon': Assets.habilities,
      //   'Widget': Container(),
      // },
      {
        'title': language.menuButton3,
        'icon': Assets.openTreasureChest,
        'Widget': Container(),
      },
      // {
      //   'title': language.menuButton4,
      //   'icon': Assets.ram,
      //   'Widget': Container(),
      // },
      // {
      //   'title': language.menuButton5,
      //   'icon': Assets.arrowScope,
      //   'Widget': Container(),
      // },
      // {
      //   'title': Esp.menuButton2,
      //   'icon': Icons.event,
      //   'Widget': const Text(Esp.menuButton2),
      // },
      // {
      //   'title': Esp.menuButton3,
      //   'icon': Icons.settings,
      //   'Widget': const Text(Esp.menuButton3),
      // },
      // {
      //   'title': Esp.menuButton4,
      //   'icon': Icons.settings,
      //   'Widget': const Text(Esp.menuButton3),
      // },
      // {
      //   'title': Esp.menuButton5,
      //   'icon': Icons.settings,
      //   'Widget': const Text(Esp.menuButton3),
      // },
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: menuProvider.createMenu(),
    );
  }
}
