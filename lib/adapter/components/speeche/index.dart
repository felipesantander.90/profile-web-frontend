import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/components/speeche/index.dart';
import 'package:template_flutter_web/loaders/components/speeche/index.dart';
import 'package:template_flutter_web/models/speeche.dart';
import 'package:template_flutter_web/providers/language.dart';
import 'package:template_flutter_web/providers/user.dart';

import '../../../validators/api_response.dart';

class SpeecheAdapter extends StatefulWidget {
  final String speecheId;
  const SpeecheAdapter({super.key, required this.speecheId});

  @override
  State<SpeecheAdapter> createState() => _SpeecheAdapterState();
}

class _SpeecheAdapterState extends State<SpeecheAdapter> {
  @override
  Widget build(BuildContext context) {
    SpeecheModel? speeche;
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    LanguageProvider languageProvider =
        Provider.of<LanguageProvider>(context, listen: false);
    return FutureBuilder(
      future: userProvider.getInfoUserSpeeche(
          languageProvider.getLanguage(), widget.speecheId),
      builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
        var validateWidget =
            snapshotValidation(snapshot, '/profile', const SpeecheLoader());
        if (validateWidget == null) {
          speeche = userProvider.makeUserSpeeche(snapshot.data!);
        }
        return AnimatedSwitcher(
            switchInCurve: Curves.easeInQuart,
            duration: const Duration(milliseconds: 1500),
            child: validateWidget ??
                Speeche(
                  speeche: speeche,
                ));
      },
    );
  }
}
