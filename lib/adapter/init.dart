import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/main.dart';
import 'package:template_flutter_web/strings/index.dart';

import '../providers/language.dart';

class InitAdapter extends StatelessWidget {
  const InitAdapter({super.key});

  @override
  Widget build(BuildContext context) {
    LanguageProvider languageProvider =
        Provider.of<LanguageProvider>(context, listen: true);
    LanguageCore language = languageProvider.getLanguageSelected();
    return InitApp(
      languageActive: language,
    );
  }
}
