import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/providers/language.dart';
import 'package:template_flutter_web/views/home/index.dart';
import 'package:template_flutter_web/views/splashScreen/body.dart';

import '../../validators/api_response.dart';

class SplashScreenAdapter extends StatefulWidget {
  const SplashScreenAdapter({super.key});

  @override
  State<SplashScreenAdapter> createState() => _SplashScreenAdapterState();
}

class _SplashScreenAdapterState extends State<SplashScreenAdapter> {
  @override
  Widget build(BuildContext context) {
    LanguageProvider languageProvider =
        Provider.of<LanguageProvider>(context, listen: false);
    return FutureBuilder(
        future: languageProvider.fetchLanguage(),
        builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
          var validateWidget =
              snapshotValidation(snapshot, '/', const BodySplashScreen());
          if (validateWidget == null) {
            languageProvider.saveLanguage(snapshot.data!);
          }
          return validateWidget ?? const Dashboard();
        });
  }
}
