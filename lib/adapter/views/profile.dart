import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/models/contact.dart';
import 'package:template_flutter_web/models/user.dart';
import 'package:template_flutter_web/providers/language.dart';
import 'package:template_flutter_web/validators/api_response.dart';
import 'package:template_flutter_web/views/profile/body.dart';

import '../../loaders/views/profile.dart';
import '../../providers/user.dart';

class ProfileAdapter extends StatefulWidget {
  const ProfileAdapter({super.key});

  @override
  State<ProfileAdapter> createState() => _ProfileAdapterState();
}

class _ProfileAdapterState extends State<ProfileAdapter> {
  @override
  Widget build(BuildContext context) {
    UserModel? userProfile;
    List<Contact> userContacts = [];
    LanguageProvider languageProvider =
        Provider.of<LanguageProvider>(context, listen: true);
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: true);
    return FutureBuilder(
      future: userProvider.getUserInfo(languageProvider.getLanguage()),
      builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
        var validateWidget =
            snapshotValidation(snapshot, '/profile', const BodyLoading());
        if (validateWidget == null) {
          userProvider.saveInfoUser(snapshot.data!);
          userProfile = userProvider.makeInfoUSer();
          userContacts = userProvider.makeUserContacts();
          userProvider.makeUserSpeeches();
        }
        return AnimatedSwitcher(
            duration: const Duration(milliseconds: 1500),
            child: validateWidget ??
                Body(
                  userProfile: userProfile,
                  userContacts: userContacts,
                  language: languageProvider.getLanguageSelected(),
                ));
      },
    );
  }
}
